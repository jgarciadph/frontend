import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidationErrors } from 'src/app/core/constants/validation-errors.constants';
import { User } from 'src/app/core/models/user.model';
import { AuthService } from 'src/app/core/services/http/auth.service';
import { UserService } from 'src/app/core/services/http/user.service';

@Component({
  selector: 'edit-user-data',
  templateUrl: './edit-user-data.component.html',
  styleUrls: ['./edit-user-data.component.css']
})
export class EditUserDataComponent implements OnInit {
  user: any;

  constructor(private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUser().subscribe(result => {
      this.user = <User>result;
    }, (err) => {
      console.log(err)
    })
  }

}
