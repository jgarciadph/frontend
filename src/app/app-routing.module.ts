import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './core/components/pagenotfound/pagenotfound.component';
import { AuthenticationModule } from './features/authentication/authentication.module';
import { CreateAdvertsModule } from './features/create-adverts/create-adverts.module';
import { EditUserDataModule } from './features/edit-user-data/edit-user-data.module';
import { SeeHomesModule } from './features/see-homes/see-homes.module';
import { InitialComponent } from './pages/initial/initial.component';

const routes: Routes = [
  { path: '', component: InitialComponent },
  {
    path: 'auth',
    loadChildren: () => import('./features/authentication/authentication.module')
      .then(element => element.AuthenticationModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./features/edit-user-data/edit-user-data.module')
      .then(element => element.EditUserDataModule)
  },
  {
    path: 'see',
    loadChildren: () => import('./features/see-homes/see-homes.module')
      .then(element => element.SeeHomesModule)
  },
  {
    path: 'advert',
    loadChildren: () => import('./features/create-adverts/create-adverts.module')
      .then(element => element.CreateAdvertsModule)
  },
  { 
    path: '404', 
    component: PageNotFoundComponent 
  },
  { 
    path: '**', 
    redirectTo: '404'
  },
];

@NgModule({
  imports: [
    AuthenticationModule,
    EditUserDataModule,
    SeeHomesModule,
    CreateAdvertsModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
