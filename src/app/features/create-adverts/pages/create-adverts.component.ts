import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Advert } from 'src/app/core/models/advert.model';
import { AdvertService } from 'src/app/core/services/http/advert.service';
import { PhotoService } from 'src/app/core/services/http/photo.service';

@Component({
  selector: 'create-adverts',
  templateUrl: './create-adverts.component.html',
  styleUrls: ['./create-adverts.component.css']
})
export class CreateAdvertsComponent implements OnInit {
  advert: any = {};
  images: Array<any> = [];

  constructor(
    private advertService: AdvertService,
    private photoService: PhotoService) { }

  ngOnInit(): void {
  }

  addNewAdvertButtonClicked() {
    //validate
    // no vacío: titulo, descripción, dirección, ciudad, páis, precio, metros, habitaciones, baños,
    // checkboxs por defecto no seleccionados
    // precio garaje no obligatorio si garaje no marcado


    console.log(this.advert)
    console.log(this.images)
    this.advertService.createAdvert(this.advert).subscribe((result) => {
      this.photoService.postPhotos(result, this.images).subscribe(data => {
        console.log(data);
        console.log("advert created")
      });

    }, (err) => {

    })
  }

  onFileChange(event: any) {
    this.images = event.target.files;
    console.log(event);
  }

  onCheckboxChange(key: any, event: any) {
    const isChecked = (<HTMLInputElement>event.target).checked;

    switch (key) {
      case "terrace": this.advert.terrace = isChecked ? 1 : 0; break;
      case "garage": this.advert.garage = isChecked ? 1 : 0; break;
      case "balcony": this.advert.balcony = isChecked ? 1 : 0; break;
      case "furnished": this.advert.furnished = isChecked ? 1 : 0; break;
    }
    console.log(key)
    console.log(event)
    console.log(this.advert)
  }

}
