import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user.model';
import { ApiPaths } from '../../constants/api-paths.constants';
import { catchError, throwError } from 'rxjs';
import { StorageService } from './storage.service';
import { StorageKeys } from '../../constants/storage-keys.constants';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient,
    private storageService: StorageService) { }

  getAuthToken() {
    return this.storageService.getData(StorageKeys.JWT_TOKEN);
  }

  setAuthToken(token: string) {
    this.storageService.setData(StorageKeys.JWT_TOKEN, token);
  }

  login(email: string, password: string) {
    return this.http.post<any>(environment.API_ENDPOINT + ApiPaths.LOGIN_PATH, { "email": email, "password": password });
  }

  register(email: string, password: string) {
    return this.http.post<any>(environment.API_ENDPOINT + ApiPaths.REGISTER_PATH, { "email": email, "password": password });
  }
}