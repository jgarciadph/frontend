import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiPaths } from 'src/app/core/constants/api-paths.constants';
import { Advert } from 'src/app/core/models/advert.model';
import { User } from 'src/app/core/models/user.model';
import { AdvertService } from 'src/app/core/services/http/advert.service';
import { PhotoService } from 'src/app/core/services/http/photo.service';
import { UserService } from 'src/app/core/services/http/user.service';
import { ImagesConfig } from 'src/app/shared/config/images.config.utils';
import { MapConfig } from 'src/app/shared/config/map.config.utils';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'initial',
  templateUrl: './initial.component.html',
  styleUrls: ['./initial.component.css']
})
export class InitialComponent implements OnInit, AfterViewInit {
  user: any;
  latestAdverts: Array<any> = [];
  ubications: Array<string> = ["Badajoz", "Caceres", "Albacete", "Salamanca"];//ubicaciones se cargaran de la BD de las ciudades que tengamos

  map: any;
  @ViewChild('map') mapElement: any;
  markers: Array<google.maps.Marker> = [];


  constructor(
    private router: Router,
    private userService: UserService,
    private advertService: AdvertService,
    private photoService: PhotoService) { }

  ngOnInit(): void {
    this.ubications.sort();
    this.ubications.unshift("Cerca de mí");

    this.userService.getUser().subscribe(result => {
      this.user = <User>result;
    }, (err) => {
      console.log(err)
    })

    this.advertService.getAdvertsLatest().subscribe(result => {
      this._loadAdverts(result)
    }, (err) => {
      console.log(err)
    })
  }
  

  ngAfterViewInit(): void {
    this.map = MapConfig.loadMapConfiguration(this.map, this.mapElement)
    if(this.latestAdverts != undefined) {
      this.latestAdverts.forEach((advert: { lat: any; lng: any; }) => {
        MapConfig.createMarker(this.map, {lat: advert.lat, lng: advert.lng}, advert);
      })
    }
  }

  _loadAdverts(advertsToLoad: Array<Advert>) {
    MapConfig.clearMapOfMarkers(this.markers);
    advertsToLoad.forEach((advert: { lat: any; lng: any; }) => {
      this.markers.push(MapConfig.createMarker(this.map, { lat: advert.lat, lng: advert.lng }, advert));
    })

    this._setPlaceholderImage(advertsToLoad);

    for (let i = 0; i < advertsToLoad.length; i++) {
      this.photoService.getPhotosFromAdvert(advertsToLoad[i].id).subscribe(responsePhotos => {
        this._setPhotosInPhotosUrl(advertsToLoad[i], responsePhotos);
        this.latestAdverts.push(advertsToLoad[i]);
      }, (err) => {
        this.latestAdverts.push(advertsToLoad[i]);
      })
    }
  }
  
  _setPlaceholderImage(adverts: Array<Advert>) {
    adverts.forEach((advert) => {
      advert.photosUrl = [ImagesConfig.ADVERT_PLACEHOLDER_IMAGE];
    })
  }

  _setPhotosInPhotosUrl(advert: Advert, responsePhotos: Array<any>) {
    advert.photosUrl = [];
    responsePhotos.forEach((photoUrl) => {
      advert.photosUrl.push((environment.API_ENDPOINT + ApiPaths.ADVERT_PHOTO_DOWNLOAD_PATH).replace(":advertId", advert.id.toString()).replace(":imagePath", photoUrl))
    })
  }

  goToDetail(advert: Advert) {
    this.router.navigate(["/see/adverts/" + advert.id])
  }

  getCitiesFromInputText() {
    let text = (<HTMLInputElement>document.getElementById('ubication')).value;
    this.ubications.filter(element => element.includes(text)).sort()
    if(!this.ubications.includes("Cerca de mí")){
      this.ubications.unshift("Cerca de mí")
    }
  }

  searchHomesInCity(event: any){
    //console.log(event.target.value);
    let cities = this.ubications.map((city) => city.toLowerCase());
    if(event.target.value == 'Cerca de mí') {
      var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };

      navigator.geolocation.getCurrentPosition((pos) => { 
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ location: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude) }, (result, status) => {
          if(result != null){
            this.router.navigate(['see/adverts'], { queryParams: {search: result[0].address_components[2].short_name}});
          }
        })
      }, (error) => {
        console.log(error)
      }, options);

    } else if(cities.includes(event.target.value.toLowerCase())) {
      this.router.navigate(['see/adverts'], { queryParams: {search: event.target.value}});
    }  
  } 
  

  


}
