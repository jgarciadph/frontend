import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiPaths } from 'src/app/core/constants/api-paths.constants';
import { Advert } from 'src/app/core/models/advert.model';
import { AdvertService } from 'src/app/core/services/http/advert.service';
import { PhotoService } from 'src/app/core/services/http/photo.service';
import { ImagesConfig } from 'src/app/shared/config/images.config.utils';
import { MapConfig } from 'src/app/shared/config/map.config.utils';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'coverimage-advert-detail',
  templateUrl: './coverimage.component.html',
  styleUrls: ['./coverimage.component.css']
})
export class CoverImageComponent implements OnInit {
  @Input() classes: string = '';
  @Input() advertId = '';

  public photos: Array<String> = []
  
  constructor(
    private photoService: PhotoService
  ) { }

  ngOnInit(): void {
    const advertId = parseInt(this.advertId)
    this.photoService.getPhotosFromAdvert(advertId).subscribe(responsePhotos => {
      this._getPhotosUrl(advertId, responsePhotos);
    });
  }

  _getPhotosUrl(advertId: number, responsePhotos: Array<any>) {
    responsePhotos.forEach((photoUrl) => {
      const url = (environment.API_ENDPOINT + ApiPaths.ADVERT_PHOTO_DOWNLOAD_PATH)
        .replace(":advertId", advertId.toString())
        .replace(":imagePath", photoUrl);

      this.photos.push(url);
    })
  }
}
