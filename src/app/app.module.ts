import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AuthenticationModule } from './features/authentication/authentication.module';
import { AppComponent } from './pages/app.component';
import { AuthService } from './core/services/http/auth.service';
import { StorageService } from './core/services/http/storage.service';
import { SharedModule } from './shared/shared.module';
import { UserService } from './core/services/http/user.service';
import { AdvertService } from './core/services/http/advert.service';
import { InitialComponent } from './pages/initial/initial.component';
import { EditUserDataModule } from './features/edit-user-data/edit-user-data.module';
import { GoogleMapsModule } from '@angular/google-maps'
import { SeeHomesModule } from './features/see-homes/see-homes.module';
import { PhotoService } from './core/services/http/photo.service';
import { CreateAdvertsModule } from './features/create-adverts/create-adverts.module';

@NgModule({
  declarations: [
    AppComponent,
    InitialComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    GoogleMapsModule,
    FormsModule,
    SharedModule,
    AuthenticationModule,
    EditUserDataModule,
    SeeHomesModule,
    CreateAdvertsModule
  ],
  providers: [
    AuthService,
    StorageService,
    UserService,
    AdvertService,
    PhotoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
