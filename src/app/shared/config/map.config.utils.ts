export class MapConfig {
    public static CENTER = { lat: 40.16826630215433, lng: -4.034587058678015 };
    public static ZOOM = 6;
    public static STYLES = [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "featureType": "administrative",
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dadada"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "road.local",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "transit",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c9c9c9"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        }
    ];
    public static infoWindow: google.maps.InfoWindow;
    public static lastAdvertClicked = '';

    static createMarker(map: any, location: any, advert: any) {
        var marker = new google.maps.Marker({
            icon: {
                scaledSize: new google.maps.Size(30, 30),
                size: new google.maps.Size(30, 30),
                url: "/assets/icons/icon-map-home-2.png"
            },
            position: new google.maps.LatLng(location.lat, location.lng),
            map: map
        });

        google.maps.event.addListener(map, 'zoom_changed', () => {
            MapConfig.resizeMarkerIcon(map, marker);
        });

        google.maps.event.addListener(marker, 'click', () => {
            MapConfig.highlightCard(advert)
            console.log("marker clicked")
        });

        return marker;
    }

    static loadMapConfiguration(map: any, mapElement: any) {
        const mapProperties = { styles: MapConfig.STYLES, center: MapConfig.CENTER, zoom: MapConfig.ZOOM };
        map = new google.maps.Map(mapElement.nativeElement, mapProperties);
        MapConfig.infoWindow = new google.maps.InfoWindow();

        return map;
    }

    static clearMapOfMarkers(markers: Array<google.maps.Marker>) {
        markers.forEach(marker => {
            marker.setMap(null);
        });
    }

    static resizeMarkerIcon(map: any, marker: google.maps.Marker) {
        var pixelSizeAtZoom0 = 30;
        var maxPixelSize = 40;

        var zoom = map.getZoom();
        var relativePixelSize = Math.round(pixelSizeAtZoom0 * Math.pow(2, zoom));

        if (relativePixelSize > maxPixelSize)
            relativePixelSize = maxPixelSize;

        marker.setIcon({
            url: "/assets/icons/icon-map-home-2.png",
            scaledSize: new google.maps.Size(relativePixelSize, relativePixelSize)
        });
    }

    static highlightCard(advert: any) {
        if(MapConfig.lastAdvertClicked){
            var card = document.getElementById("card"+ MapConfig.lastAdvertClicked)
            card?.setAttribute("style", "transform: scale(1)");
            card?.getElementsByClassName("card-homes-body")[0].setAttribute("style", "")
            document.getElementById("carousel" + MapConfig.lastAdvertClicked)?.setAttribute("style", "")
        }

        MapConfig.lastAdvertClicked = advert.id.toString();
        var card = document.getElementById("card"+ advert.id)
        card?.setAttribute("style", "transform: scale(1.1)");
        card?.getElementsByClassName("card-homes-body")[0].setAttribute("style", "box-shadow: 0px 0px 10px 0px rgb(255 193 7 / 60%);")
        document.getElementById("carousel" + advert.id)?.setAttribute("style", "box-shadow: 0px 0px 10px 0px rgb(255 193 7 / 60%);")
    }

    static getInfoWindowContent(advert: any) {
        var indicators = ''
        var imgDivs = ''
        for (var i = 0; i < advert.photosUrl.length; i++) {
            var photoUrl = advert.photosUrl[i];
            indicators = indicators.concat(`<button type="button" attr.data-bs-target="#carousel${advert.id}_marker" attr.data-bs-slide-to="${i}" class="${(i == 0) ? 'active' : ''}" attr.aria-current="${(i == 0) ? 'true' : 'false'}" attr.aria-label="Slide ${i}"></button>`)
            imgDivs = imgDivs.concat(`<div class="${(i == 0) ? 'carousel-item active' : 'carousel-item'}"><img src="${photoUrl}" class="d-block w-100" alt="..."></div>`)
        }
        var content = 
            ` 
            <div id="carousel${advert.id}_marker" class="carousel slide card-homes-img" data-bs-ride="carousel" data-bs-keyboard="true">
                <div class="carousel-indicators">
                    ${indicators}
                </div>
                <div class="carousel-inner">
                    ${imgDivs}
                </div>
                <button class="carousel-control-prev" type="button" attr.data-bs-target="#carousel${advert.id}_marker" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" attr.data-bs-target="#carousel${advert.id}_marker" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            `
        return content;
    }
}