import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Advert } from 'src/app/core/models/advert.model';
import { AdvertService } from 'src/app/core/services/http/advert.service';

@Component({
  selector: 'advert-detail',
  templateUrl: './advert-detail.component.html',
  styleUrls: ['./advert-detail.component.css']
})
export class AdvertDetailComponent implements OnInit {
  advert: any;
  
  constructor(
    private route: ActivatedRoute,
    private advertService: AdvertService
  ) { }

  ngOnInit(): void {
    const advertId = this.route.snapshot.paramMap.get('id');
    if(advertId != null) 
      this.advertService.getAdvertsById(advertId).subscribe(advert => {
        this.advert = advert;
      })
  }

}
