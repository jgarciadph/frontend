import { ModuleWithProviders } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { EditUserDataModule } from "./edit-user-data.module"
import { EditUserDataComponent } from "./pages/edit-data/edit-user-data.component"

export const routes: Routes = [
    { path: 'user/edit', component: EditUserDataComponent },
]

export const routing: ModuleWithProviders<EditUserDataModule> = RouterModule.forChild(routes)