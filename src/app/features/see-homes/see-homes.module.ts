import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { routing } from './see-homes-routing.module';
import { SeeHomesComponent } from './pages/see-homes/see-homes.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { AdvertDetailComponent } from './pages/detail/advert-detail.component';
import { CoverImageComponent } from './pages/detail/components/coverimage/coverimage.component';
import { SummaryComponent } from './pages/detail/components/summary/summary.component';
import { MapComponent } from './pages/detail/components/map/map.component';
import { StorageService } from 'src/app/core/services/http/storage.service';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        GoogleMapsModule,
        routing
    ],
    declarations: [
        SeeHomesComponent,
        AdvertDetailComponent,
        CoverImageComponent,
        SummaryComponent,
        MapComponent
    ],
    exports: [
        SeeHomesComponent,
        AdvertDetailComponent
    ]
})
export class SeeHomesModule { }