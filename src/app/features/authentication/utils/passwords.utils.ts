import { ValidationErrors } from "src/app/core/constants/validation-errors.constants";

export class PasswordUtils {
    public static validatePassword(password: string, errors: Array<string>) {
        var lowerCaseLetters = /[a-z]/g;
        if (!password.match(lowerCaseLetters)) {
            errors.push(ValidationErrors.ERR_NO_LOWERCASE_IN_PASS);
        }

        var upperCaseLetters = /[A-Z]/g;
        if (!password.match(upperCaseLetters)) {
            errors.push(ValidationErrors.ERR_NO_UPPERCASE_IN_PASS);
        } 

        var numbers = /[0-9]/g;
        if (!password.match(numbers)) {
            errors.push(ValidationErrors.ERR_NO_NUMBERS_IN_PASS);
        }

        if (password.length < 8) {
            errors.push(ValidationErrors.ERR_NO_LENGTH_IN_PASS);
        }

        return errors;
    }

    // To show a level bar with the level of password
    public static getLevelOfPassword(password: string) {
        var errors = this.validatePassword(password, []);
        return 4 - errors.length;
    }
}