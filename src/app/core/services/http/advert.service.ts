import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { ApiPaths } from '../../constants/api-paths.constants';
import { HeadersUtils } from '../../utils/headers.utils';

@Injectable()
export class AdvertService {
  constructor(private http: HttpClient, 
    private authService: AuthService) { }

  getAdvertsFromUser() {
    return this.http.get<any>(environment.API_ENDPOINT + ApiPaths.ADVERT_PATH, { headers: HeadersUtils.getHeaders(this.authService) });
  }

  getAdvertsById(advertId: string) {
    return this.http.get<any>(environment.API_ENDPOINT + ApiPaths.ADVERT_ID_PATH.replace(":advertId", advertId), { headers: HeadersUtils.getHeaders(this.authService) });
  }

  getAdvertsLatest() {
    return this.http.get<any>(environment.API_ENDPOINT + ApiPaths.ADVERT_LATEST_PATH, { headers: HeadersUtils.getHeaders(this.authService) });
  }

  getAdvertsBySearch(direction: string) {
    return this.http.get<any>(environment.API_ENDPOINT + ApiPaths.ADVERT_SEARCH_PATH, { params: { address: direction }, headers: HeadersUtils.getHeaders(this.authService) });
  }

  getAdvertsRecommended() {
    return this.http.get<any>(environment.API_ENDPOINT + ApiPaths.ADVERT_RECOMMENDED_PATH, { headers: HeadersUtils.getHeaders(this.authService) });
  }

  createAdvert(advert: any) {
    return this.http.post<any>(environment.API_ENDPOINT + ApiPaths.ADVERT_PATH, advert,{ headers: HeadersUtils.getHeaders(this.authService) });
  }
}