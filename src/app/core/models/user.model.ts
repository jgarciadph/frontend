export interface User {
    id: number;
    email : string;
    password : string;
    name : string;
    lastname : string;
    telephone : string;
    created_at : string;
    birthday : string;
    direction : string;
    city : string;
    country : string;
    photo : string;
}