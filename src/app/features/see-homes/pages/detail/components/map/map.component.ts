import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiPaths } from 'src/app/core/constants/api-paths.constants';
import { Advert } from 'src/app/core/models/advert.model';
import { AdvertService } from 'src/app/core/services/http/advert.service';
import { PhotoService } from 'src/app/core/services/http/photo.service';
import { ImagesConfig } from 'src/app/shared/config/images.config.utils';
import { MapConfig } from 'src/app/shared/config/map.config.utils';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'map-advert-detail',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, AfterViewInit {
  @Input() classes: string = '';
  @Input() advert: any;

  map: any;
  @ViewChild('map') mapElement: any;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this._loadMap();
  }

  _loadMap() {
    this.map = MapConfig.loadMapConfiguration(this.map, this.mapElement)
    this._setMarker();
  }

  _setMarker() {
    MapConfig.createMarker(this.map, { lat: this.advert.lat, lng: this.advert.lng }, this.advert);
  }
}
