import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthenticationComponent } from './authentication.component'
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { routing } from './authentication-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        FormsModule,
        CommonModule, 
        routing
    ],
    declarations: [
        AuthenticationComponent, LoginComponent, RegisterComponent
    ],
    exports: [
        AuthenticationComponent, LoginComponent, RegisterComponent
    ]
})
export class AuthenticationModule { }