import { ModuleWithProviders } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { AuthenticationModule } from "./authentication.module"
import { LoginComponent } from "./pages/login/login.component"
import { RegisterComponent } from "./pages/register/register.component"

export const routes: Routes = [
    { path: 'auth/login', component: LoginComponent },
    { path: 'auth/register', component: RegisterComponent }
  ]
  
  export const routing: ModuleWithProviders<AuthenticationModule> = RouterModule.forChild(routes)