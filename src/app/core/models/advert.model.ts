export interface Advert {
    id: number,
    owner: number,
    title: string,
    description: string,
    direction: string,
    city: string,
    country: string,
    pricepermonth: number,
    meters: number,
    rooms: number,
    floor: number,
    pricegarage: number,
    bathrooms: number,
    garage: number,
    balcony: number,
    terrace: number,
    furnished: number,
    created_at: string,
    updated_at: string,
    lat: number,
    lng: number,
    photosUrl: Array<string>,
}