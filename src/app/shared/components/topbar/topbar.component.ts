import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent } from '@angular/router';
import { StorageKeys } from 'src/app/core/constants/storage-keys.constants';
import { AuthService } from 'src/app/core/services/http/auth.service';
import { StorageService } from 'src/app/core/services/http/storage.service';

@Component({
  selector: 'topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {
  //design
  scrolled = false;
  fixedTopbar = false;
  currentUrl = '/';

  loginLinkVisible = false;
  logoutLinkVisible = false;
  registerLinkVisible = false;
  editUserLinkVisible = false;
  seeHomesUserLinkVisible = false;
  createAdvertLinkVisible = false;

  constructor(private router: Router,
    private authService: AuthService,
    private storageService: StorageService) { }

  ngOnInit(): void {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationStart) {
        console.log(event)
      }

      if (event instanceof NavigationEnd) {
        console.log(event)
        this.currentUrl = this.router.url;
        this.checkIfMustFixedBar();
        this.checkTopBarElementsToShow();
      }

      if (event instanceof NavigationError) {
        console.log(event)
      }
    });
  }

  checkTopBarElementsToShow() {
    if (Object.keys(this.authService.getAuthToken()).length != 0) { //TODO: Reload auth service to must be secure
      this.setTopBarElementsLogged();
    } else {
      this.setTopBarElementsUnLogged();
    }
  }

  setTopBarElementsUnLogged() {
    this.loginLinkVisible = true;
    this.registerLinkVisible = true;
    this.editUserLinkVisible = false;
    this.seeHomesUserLinkVisible = true;
    this.createAdvertLinkVisible = false;
    this.logoutLinkVisible = false;
  }

  setTopBarElementsLogged() {
    this.loginLinkVisible = false;
    this.registerLinkVisible = false;
    this.editUserLinkVisible = true;
    this.seeHomesUserLinkVisible = true;
    this.createAdvertLinkVisible = true;
    this.logoutLinkVisible = true;
  }

  checkIfMustFixedBar() {
    const routesWithFixedBar = ['/', '/auth/login', '/auth/register', '/404']
    if (routesWithFixedBar.includes(this.router.url)) {
      this.fixedTopbar = true;
    } else {
      this.fixedTopbar = false;
    }
  }

  logout() {
    this.storageService.removeData(StorageKeys.JWT_TOKEN);
    window.location.reload();
  }

  @HostListener("window:scroll", ['$event']) onWindowScroll(event: Event) {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = document.documentElement.scrollHeight;
    if (pos / max - 1 < 0.10) {
      this.scrolled = false;
    } else {
      this.scrolled = true;
    }
  }
}
