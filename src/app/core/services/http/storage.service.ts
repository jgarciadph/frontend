import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class StorageService {
    constructor(private http: HttpClient) { }

    setData(key: string, data: any) {
        const jsonData = JSON.stringify(data)
        localStorage.setItem(key, jsonData)
    }

    getData(key: string) {
        try {
            return JSON.parse(localStorage.getItem(key) || '{}');
        } catch {
            return "";
        }
    }

    removeData(key: string) {
        localStorage.removeItem(key)
    }

    clear() {
        localStorage.clear()
    }
}