import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiPaths } from 'src/app/core/constants/api-paths.constants';
import { Advert } from 'src/app/core/models/advert.model';
import { AdvertService } from 'src/app/core/services/http/advert.service';
import { PhotoService } from 'src/app/core/services/http/photo.service';
import { ImagesConfig } from 'src/app/shared/config/images.config.utils';
import { MapConfig } from 'src/app/shared/config/map.config.utils';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'summary-advert-detail',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  @Input() classes: string = '';
  @Input() advert: any;

  constructor() { }

  ngOnInit(): void {
  }
}
