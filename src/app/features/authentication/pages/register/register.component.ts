import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidationErrors } from 'src/app/core/constants/validation-errors.constants';
import { AuthService } from 'src/app/core/services/http/auth.service';
import { PasswordUtils } from '../../utils/passwords.utils';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email = '';
  pass = '';
  confirmPass = '';
  errors: Array<string> = [];

  constructor(private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void { }

  onRegisterButtonPressed() {
    this.errors = [];
    if (this.validateForm()) {
      this.authService.register(this.email, this.pass).subscribe((result) => {
        if (result && result['auth-token']){
          this.authService.setAuthToken(result['auth-token']);
          this.router.navigate(['']);
        }
      }, (err) => {
        this.errors.push(ValidationErrors.ERR_EMAIL_DUPLICATED)
      })
    } else {
      //show errors
      console.log(this.errors)
    }
  }

  validateForm() {
    if (!this.email) {
      this.errors.push(ValidationErrors.ERR_EMAIL_EMPTY);
    } else if(!this.checkEmailFormat(this.email)) {
      this.errors.push(ValidationErrors.ERR_EMAIL_NOT_VALID_FORMAT_EMPTY);
    }

    if (!this.pass) {
      this.errors.push(ValidationErrors.ERR_PASS_EMPTY);
    }
    if (!this.confirmPass) {
      this.errors.push(ValidationErrors.ERR_CONFIRM_PASS_EMPTY);
    }
    if (this.pass != this.confirmPass) {
      this.errors.push(ValidationErrors.ERR_PASS_CONFIRMPASS_DISTINCT);
    }

    PasswordUtils.validatePassword(this.pass, this.errors);

    return this.errors.length == 0;
  }

  checkEmailFormat(email: String) {
    return email
      .toLowerCase()
      .match(
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
  }

  get getEmailEmptyError() {
    return ValidationErrors.ERR_EMAIL_EMPTY
  }
  get getEmailNotValidFormat() {
    return ValidationErrors.ERR_EMAIL_NOT_VALID_FORMAT_EMPTY
  }
  get getEmailDuplicatedError() {
    return ValidationErrors.ERR_EMAIL_DUPLICATED
  }
  get getPassEmptyError() {
    return ValidationErrors.ERR_PASS_EMPTY
  }
  get getConfirmPassEmptyError() {
    return ValidationErrors.ERR_CONFIRM_PASS_EMPTY
  }
  get getPassConfirmPassDistinct() {
    return ValidationErrors.ERR_PASS_CONFIRMPASS_DISTINCT
  }
  
}
