import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidationErrors } from 'src/app/core/constants/validation-errors.constants';
import { AuthService } from 'src/app/core/services/http/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = '';
  pass = '';
  errors: Array<string> = [];

  constructor(private router: Router,
    private authService: AuthService) { }

  ngOnInit(): void { }

  onSignInButtonPressed() {
    this.authService.login(this.email, this.pass).subscribe((result) => {
      if (result && result['auth-token']){
        this.authService.setAuthToken(result['auth-token'])
        this.router.navigate(['']);
      }
    }, (err) => {
      this.errors.push(ValidationErrors.ERR_USER_OR_PASS_INCORRECT);
    })
  }
}
