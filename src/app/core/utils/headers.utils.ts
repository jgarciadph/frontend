import { HttpHeaders } from "@angular/common/http";
import { AuthService } from "../services/http/auth.service";

export class HeadersUtils {
    public static getHeaders(authService: AuthService, jsonType: Boolean = true) {
        const authToken = authService.getAuthToken();
        const httpHeaders =  new HttpHeaders({
          'Authorization': `Bearer ${authToken}`
        })
        if(jsonType)
          httpHeaders.set('Content-Type', 'application/json')

        return httpHeaders;
    }
}