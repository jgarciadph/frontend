export class ApiPaths {
    public static LOGIN_PATH = '/login';
    public static REGISTER_PATH = '/register';
    public static USER_PATH = '/user';  
    public static ADVERT_PATH = '/adverts';
    public static ADVERT_ID_PATH = '/adverts/:advertId';
    public static ADVERT_LATEST_PATH = this.ADVERT_PATH + '/latest';
    public static ADVERT_SEARCH_PATH = this.ADVERT_PATH + '/search';
    public static ADVERT_RECOMMENDED_PATH = this.ADVERT_PATH + '/recommended';
    public static ADVERT_PHOTOS_PATH = this.ADVERT_PATH + '/:advertId/photos';
    public static ADVERT_PHOTO_DOWNLOAD_PATH = this.ADVERT_PATH + '/:advertId/photos/:imagePath';

 }