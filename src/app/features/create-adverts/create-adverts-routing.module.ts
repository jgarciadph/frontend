import { ModuleWithProviders } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { CreateAdvertsComponent } from "./pages/create-adverts.component"
import { CreateAdvertsModule } from "./create-adverts.module"

export const routes: Routes = [
    { path: 'advert/create', component: CreateAdvertsComponent },
]

export const routing: ModuleWithProviders<CreateAdvertsModule> = RouterModule.forChild(routes)