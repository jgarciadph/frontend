import { ModuleWithProviders } from "@angular/core"
import { RouterModule, Routes } from "@angular/router"
import { AdvertDetailComponent } from "./pages/detail/advert-detail.component"
import { SeeHomesComponent } from "./pages/see-homes/see-homes.component"
import { SeeHomesModule } from "./see-homes.module"

export const routes: Routes = [
    { path: 'see/adverts', component: SeeHomesComponent },
    { path: 'see/adverts/:id', component: AdvertDetailComponent },
]

export const routing: ModuleWithProviders<SeeHomesModule> = RouterModule.forChild(routes)