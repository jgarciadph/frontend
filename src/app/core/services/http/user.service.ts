import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { ApiPaths } from '../../constants/api-paths.constants';
import { HeadersUtils } from '../../utils/headers.utils';

@Injectable()
export class UserService {
  constructor(private http: HttpClient, 
    private authService: AuthService) { }

  getUser() {
    return this.http.get<any>(environment.API_ENDPOINT + ApiPaths.USER_PATH, { headers: HeadersUtils.getHeaders(this.authService) });
  }
}