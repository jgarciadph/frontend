import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiPaths } from 'src/app/core/constants/api-paths.constants';
import { Advert } from 'src/app/core/models/advert.model';
import { AdvertService } from 'src/app/core/services/http/advert.service';
import { PhotoService } from 'src/app/core/services/http/photo.service';
import { StorageService } from 'src/app/core/services/http/storage.service';
import { ImagesConfig } from 'src/app/shared/config/images.config.utils';
import { MapConfig } from 'src/app/shared/config/map.config.utils';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'see-homes',
  templateUrl: './see-homes.component.html',
  styleUrls: ['./see-homes.component.css']
})
export class SeeHomesComponent implements OnInit, AfterViewInit {
  //Form fields 
  direction: string = "";
  price: number = 0;
  metters: number = 0;
  found: string = "";
  save: boolean = false;

  user: any;
  adverts: Array<Advert> = [];
  loaded = true;
  showInList = false;
  
  map: any;
  @ViewChild('map') mapElement: any;
  markers: Array<google.maps.Marker> = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private localStorageService: StorageService,
    private advertService: AdvertService,
    private photoService: PhotoService) {
    this.route.queryParams.subscribe(params => {
      this.direction = params['search'];
    });
  }

  ngOnInit(): void {
    if(this.localStorageService.getData("saved.search") == true){
      this.direction = this.localStorageService.getData("saved.search.direction");
      this.price = this.localStorageService.getData("saved.search.price");
      this.metters = this.localStorageService.getData("saved.search.metters");
      this.save = true;
    }

    if (this.direction) {
      this.searchInLocation()
    } else {
      this.advertService.getAdvertsLatest().subscribe(latestAdverts => {
        this._loadAdverts(latestAdverts);
      }, (err) => { this._errorHandler(err) })
    }

  }

  ngAfterViewInit(): void {
    this.map = MapConfig.loadMapConfiguration(this.map, this.mapElement)
  }

  searchInLocation() {
    var geocoder = new google.maps.Geocoder();
    this.adverts = []

    this.advertService.getAdvertsBySearch(this.direction).subscribe(advertsBySearch => {
      this._loadAdverts(advertsBySearch);
    }, error => {
      console.log(error)
    })

    geocoder.geocode({ address: this.direction }, (result, status) => {
      if (result != null)
        this.map.fitBounds(result[0].geometry.bounds)
    })
  }

  _loadAdverts(advertsToLoad: Array<Advert>) {
    this.found = advertsToLoad.length + " propiedades";

    MapConfig.clearMapOfMarkers(this.markers);
    advertsToLoad.forEach((advert: { lat: any; lng: any; }) => {
      this.markers.push(MapConfig.createMarker(this.map, { lat: advert.lat, lng: advert.lng }, advert));
    })

    this._setPlaceholderImage(advertsToLoad);

    for (let i = 0; i < advertsToLoad.length; i++) {
      this.photoService.getPhotosFromAdvert(advertsToLoad[i].id).subscribe(responsePhotos => {
        this._setPhotosInPhotosUrl(advertsToLoad[i], responsePhotos);
        this.adverts.push(advertsToLoad[i]);
      }, (err) => {
        this._errorHandler(err);
        this.adverts.push(advertsToLoad[i]);
      })
    }
  }

  goToDetail(advert: Advert) {
    this.router.navigate(["/see/adverts/" + advert.id])
  }

  showInListView(list: boolean) {
    this.showInList = list;
  }

  saveSearch(event: any) {
    if(event.srcElement.checked) {
      this.localStorageService.setData("saved.search", true);
      this.localStorageService.setData("saved.search.direction", this.direction);
      this.localStorageService.setData("saved.search.price", this.price);
      this.localStorageService.setData("saved.search.metters", this.metters);
    } else {
      this.localStorageService.removeData("saved.search");
      this.localStorageService.removeData("saved.search.direction");
      this.localStorageService.removeData("saved.search.price");
      this.localStorageService.removeData("saved.search.metters");
    }
  }

  _setPlaceholderImage(adverts: Array<Advert>) {
    adverts.forEach((advert) => {
      advert.photosUrl = [ImagesConfig.ADVERT_PLACEHOLDER_IMAGE];
    })
  }

  _setPhotosInPhotosUrl(advert: Advert, responsePhotos: Array<any>) {
    advert.photosUrl = [];
    responsePhotos.forEach((photoUrl) => {
      advert.photosUrl.push((environment.API_ENDPOINT + ApiPaths.ADVERT_PHOTO_DOWNLOAD_PATH).replace(":advertId", advert.id.toString()).replace(":imagePath", photoUrl))
    })
  }

  _errorHandler(err: any) {
    console.log(err)
  }

}
