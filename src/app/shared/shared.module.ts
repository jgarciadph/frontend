import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './components/footer/footer.component';
import { TopbarComponent } from './components/topbar/topbar.component';

@NgModule({
    imports: [
        CommonModule, 
    ],
    declarations: [
        TopbarComponent,
        FooterComponent
    ],
    exports: [
        TopbarComponent,
        FooterComponent
    ]
})
export class SharedModule { }