import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidationErrors } from 'src/app/core/constants/validation-errors.constants';
import { AuthService } from 'src/app/core/services/http/auth.service';

@Component({
  selector: 'pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrls: ['./pagenotfound.component.css']
})
export class PageNotFoundComponent implements OnInit {
  constructor() { }

  ngOnInit(): void { }
}
