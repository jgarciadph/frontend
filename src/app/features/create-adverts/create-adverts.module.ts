import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { routing } from './create-adverts-routing.module';
import { CreateAdvertsComponent } from './pages/create-adverts.component';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        GoogleMapsModule,
        routing
    ],
    declarations: [
        CreateAdvertsComponent
    ],
    exports: [
        CreateAdvertsComponent
    ]
})
export class CreateAdvertsModule { }