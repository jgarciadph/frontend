export class ValidationErrors {
    //Register
    public static ERR_PASS_CONFIRMPASS_DISTINCT = 'validation_error_err_pass_confirmpass_distinct';
    public static ERR_EMAIL_NOT_VALID_FORMAT_EMPTY = 'validation_error_err_email_not_valid_format';
    public static ERR_EMAIL_EMPTY = 'validation_error_err_email_empty';
    public static ERR_PASS_EMPTY = 'validation_error_err_pass_empty';
    public static ERR_CONFIRM_PASS_EMPTY = 'validation_error_err_confirm_pass_empty';
    public static ERR_EMAIL_DUPLICATED = 'validation_error_err_mail_duplicated';
    public static ERR_NO_LOWERCASE_IN_PASS = 'validation_error_err_no_lowercase_in_pass';
    public static ERR_NO_UPPERCASE_IN_PASS = 'validation_error_err_no_uppercase_in_pass';
    public static ERR_NO_NUMBERS_IN_PASS = 'validation_error_err_no_numbers_in_pass';
    public static ERR_NO_LENGTH_IN_PASS = 'validation_error_err_no_length_in_pass';

    //Login
    public static ERR_USER_OR_PASS_INCORRECT = 'validation_error_err_or_pass_incorrect';
 }