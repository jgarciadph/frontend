import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from 'src/environments/environment';
import { ApiPaths } from '../../constants/api-paths.constants';
import { HeadersUtils } from '../../utils/headers.utils';

@Injectable()
export class PhotoService {
  constructor(private http: HttpClient, 
    private authService: AuthService) { }

  getPhotosFromAdvert(advertId: number) {
    return this.http.get<any>(environment.API_ENDPOINT + ApiPaths.ADVERT_PHOTOS_PATH.replace(":advertId", advertId.toString()), { headers: HeadersUtils.getHeaders(this.authService) });
  }

  postPhotos(advertId: number, photos: Array<any>) {
    let formData = new FormData();
    console.log(photos)
    for(var i=0; i<photos.length; i++) {
      formData.append('photos', photos[i]);
    }

    return this.http.post(environment.API_ENDPOINT + ApiPaths.ADVERT_PHOTOS_PATH.replace(":advertId", advertId.toString()), formData, { headers: HeadersUtils.getHeaders(this.authService, false) })
  }
}