import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { EditUserDataComponent } from './pages/edit-data/edit-user-data.component';
import { routing } from './edit-user-data.routing.module';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        routing
    ],
    declarations: [
        EditUserDataComponent
    ],
    exports: [
        EditUserDataComponent
    ]
})
export class EditUserDataModule { }